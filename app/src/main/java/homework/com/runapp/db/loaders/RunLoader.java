package homework.com.runapp.db.loaders;

import android.content.Context;

import homework.com.runapp.RunManager;
import homework.com.runapp.utils.Run;

public class RunLoader extends DataLoader<Run> {

    private long mRunId;

    public RunLoader(Context context, long runId) {
        super(context);
        mRunId = runId;
    }

    @Override
    public Run loadInBackground() {
        return RunManager.get(getContext()).getRun(mRunId);
    }
}

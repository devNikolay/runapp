package homework.com.runapp.activity;

import android.support.v4.app.Fragment;

import homework.com.runapp.fragments.RunFragment;

public class RunActivity extends SingleFragmentActivity {
    public static final String EXTRA_RUN_ID =
            "homework.com.runapp.run_id";

    @Override
    protected Fragment createFragment() {
//        return new RunFragment();
        long runId = getIntent().getLongExtra(EXTRA_RUN_ID, -1);
        if (runId != -1) {
            return RunFragment.newInstance(runId);
        } else {
            return new RunFragment();
        }
    }
}

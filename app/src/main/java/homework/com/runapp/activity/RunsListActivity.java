package homework.com.runapp.activity;

import android.support.v4.app.Fragment;

import homework.com.runapp.fragments.RunListFragment;


public class RunsListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new RunListFragment();
    }
}

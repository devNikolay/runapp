package homework.com.runapp.receiver;

import android.content.Context;
import android.location.Location;

import homework.com.runapp.RunManager;

public class TrackingLocationReceiver extends LocationReceiver {

    @Override
    public void onLocationReceived(Context c, Location loc) {
        RunManager.get(c).insertLocation(loc);
    }
}

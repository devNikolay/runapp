package homework.com.runapp.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import homework.com.runapp.R;
import homework.com.runapp.db.RunDatabaseHelper;
import homework.com.runapp.utils.Run;

public class RunCursorAdapter extends CursorAdapter {

    private RunDatabaseHelper.RunCursor mRunCursor;

    public RunCursorAdapter(Context context, RunDatabaseHelper.RunCursor cursor) {
        super(context, cursor, 0);
        mRunCursor = cursor;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Run run = mRunCursor.getRun();
        TextView startDateTextview = (TextView) view;
        String cellText = context.getString(R.string.cell_text, run.getStartDate());
        startDateTextview.setText(cellText);

    }
}
